"""Moodle autograder package.

This file contains initialization for logging.
This includes specifying a "silly" log level.
"""
import logging
import logging.config
import os
from functools import partial
from typing import TYPE_CHECKING, Iterable, Tuple

import yaml

if TYPE_CHECKING:
    import bs4.element
    from bs4 import BeautifulSoup

    bs4.element.Tag = BeautifulSoup

LOG_CFG_FILE = "logging.yaml"
_custom_levels = [
    (
        "silly",
        "SILLY",
        logging.NOTSET + 1,
    )
]


def _init_custom_levels(custom_levels: Iterable[Tuple[str, str, int]]):
    def _get_self_log_func(level):
        # define function properly
        # Was getting "W0640: Cell variable level defined in loop (cell-var-from-loop)"
        # when function was defined in loop
        # see https://stackoverflow.com/a/12423750/3578387
        def _func(self, msg, *args, **kwargs):
            self.log(level, msg, *args, **kwargs)

        return _func

    for function_name, level_name, level in custom_levels:
        logging.addLevelName(level, level_name)
        setattr(logging, function_name, partial(logging.log, level))
        setattr(logging.Logger, function_name, _get_self_log_func(level))


def init_logging():
    """Initialize logging.
    This tries to configure the logger with the config file "LOG_CFG_FILE"
    """
    # logging.config.fileconfig is (somewhat) deprecated
    # so we use dictConfig and load from a yaml file
    try:
        with open(LOG_CFG_FILE, "r") as f:
            cfg = yaml.load(f, Loader=yaml.FullLoader)
            logging.config.dictConfig(cfg)
        logging.info("Started logger using config file %s", os.path.abspath(LOG_CFG_FILE))
    except Exception:  # pylint: disable=broad-except
        logging.exception("Could not load logging config file %s", os.path.abspath(LOG_CFG_FILE))


_init_custom_levels(_custom_levels)
# If the logging config is not found, basicconfig is used.
# This does not interfere with test logging,
# as basicconfig has no effect if logging was set up before.
if not os.path.isfile(LOG_CFG_FILE):
    logging.basicConfig(
        format="%(asctime)s %(levelname)8s | %(name)s: %(message)s",
        level=os.environ.get("LOGLEVEL", "INFO"),
    )
    logging.error(
        "Did not find logging config file (%s), using basic config",
        os.path.abspath(LOG_CFG_FILE),
    )
