"""Manual test for the moodle_autograder package."""
import logging
import os

from moodle_autograder.attempt import QuestionAttempt, QuestionReport
from moodle_autograder.moodle.base_ import GradeMode
from moodle_autograder.util.base_grader import BaseGrader, Grade

from . import init_logging
from .moodle.v3_9 import Moodle_3_9 as Moodle

if __name__ == "__main__":
    init_logging()
    m = Moodle(
        "https://panda.uni-paderborn.de/",
        os.getenv("SESSION_COOKIE", ""),
        "MoodleSessionupblms",
    )

    class Grader(BaseGrader):
        def __init__(self, moodle: Moodle) -> None:
            super().__init__(moodle, quiz_id=2241408, slot=1, question_id=5925322)

        def initialize(self):
            return super().initialize()

        def deinitialize(self):
            return super().deinitialize()

        def grade_internal(self, qr: QuestionReport, qa: QuestionAttempt) -> Grade:
            logging.info("Foo Bar")
            return Grade(0.21, 1)

    g = Grader(m)
    with m.batch_grade() as _:
        g.gradeAll(grade_mode=GradeMode.ALL)
    print("DONE")
