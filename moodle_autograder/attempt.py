from dataclasses import dataclass
from enum import Enum
from typing import Dict, List, Optional

from bs4.element import Tag


class CommentFormat(Enum):
    FORMAT_MOODLE = 0
    FORMAT_HTML = 1
    FORMAT_PLAIN = 2
    FORMAT_WIKI = 3
    FORMAT_MARKDOWN = 4


class QuestionState(Enum):
    CORRECT = "correct"
    PARTIALLY_CORRECT = "partiallycorrect"
    INCORRECT = "incorrect"
    NOT_ANSWERED = "notanswered"
    COMPLETE = "complete"


@dataclass
class QuestionAttempt:
    ## Above
    attempt_number: int
    student_name: str

    ## INFO
    slot: int
    state: QuestionState
    # current_mark - in mark section
    # max_mark - in mark section

    ## formulation
    qtext: Tag

    ## comment
    current_comment: str
    comment_item_id: int
    comment_format: CommentFormat

    sequencecheck: int

    ## mark
    current_mark: Optional[float]
    max_mark: float
    min_fraction: float
    max_fraction: float

    quba_id: int


@dataclass
class EssayAttempt(QuestionAttempt):
    answer: Tag
    attachments: Dict[str, bytes]


@dataclass
class ShortanswerAttempt(QuestionAttempt):
    answer: str


@dataclass
class QuestionReport:
    quiz_id: int
    slot: int
    question_id: int
    attempts: List[QuestionAttempt]
