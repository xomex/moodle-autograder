import logging
import time
from abc import ABC, abstractmethod
from enum import Enum
from typing import Iterable

import requests

from moodle_autograder.attempt import CommentFormat, QuestionAttempt, QuestionReport

_LOGGER = logging.getLogger(__name__)


class MoodleRequestException(Exception):
    pass


class GradeResponse:
    pass


class GradeMode(Enum):
    ALL = "all"
    NEED_GRADING = "needsgrading"
    MANUALLY_GRADED = "manuallygraded"
    AUTO_GRADED = "autograded"


class Moodle(ABC):
    def __init__(self, base_url: str, cookie: str, cookie_name: str) -> None:
        # check common cookie errors - just warn the user, maybe the moodle instance is configured differently than expected :shrug:
        if not cookie:
            _LOGGER.warning("Cookie is empty - please provide one")
        if cookie.startswith(cookie_name):
            _LOGGER.warning(
                "Cookie value starts with the cookie_name - please just include the value"
            )

        while base_url.endswith("/"):
            base_url = base_url[:-1]
        self.base_url = base_url
        self.session: requests.Session = requests.session()
        self.session.cookies[cookie_name] = cookie
        self.batch_grader = None

    def _request(self, relative_url: str, method: str, **kwargs):
        skip_ok_check = kwargs.pop("SKIP_OK_CHECK", False)

        params = kwargs.pop("params", None)
        if params is None:
            params = {}
        params.setdefault("lang", "en")
        kwargs["params"] = params

        if relative_url.startswith("https://") or relative_url.startswith("http://"):
            # absolute url
            assert relative_url.startswith(self.base_url)
            url = relative_url
        else:
            url = self.base_url + relative_url

        _LOGGER.silly("Requesting %s %s", method, url)
        try:
            res = self.session.request(method, url, **kwargs)
        except requests.exceptions.TooManyRedirects as ex:
            raise MoodleRequestException(
                f"Failed to {method} {url!r}; too many redirects - maybe the cookie is invalid?"
            ) from ex
        if not skip_ok_check and not res.ok:
            # with open("_tmp.html", "wb") as f:
            #     f.write(res.content)
            raise MoodleRequestException(f"Failed to {method} {url!r}", res)

        _LOGGER.silly("Done %s %s", method, url)
        return res

    def get(self, relative_url: str, params=None, **kwargs):
        return self._request(relative_url, "GET", params=params, **kwargs)

    def post(self, relative_url: str, data=None, json=None, **kwargs):
        return self._request(relative_url, "POST", data=data, json=json, **kwargs)

    def batch_grade(self, submit_after_num=0, submit_after_seconds=0):
        assert self.batch_grader is None
        return BatchGrader(self, submit_after_num, submit_after_seconds)

    @abstractmethod
    def get_attempt_count(
        self,
        quiz_id: int,
        slot: int,
        question_id: int,
        grade_mode: GradeMode = GradeMode.NEED_GRADING,
    ) -> int:
        pass

    @abstractmethod
    def get_attempts(
        self,
        quiz_id: int,
        slot: int,
        question_id: int,
        grade_mode: GradeMode = GradeMode.NEED_GRADING,
    ) -> QuestionReport:
        pass

    @abstractmethod
    def post_grades(self, grades: Iterable[GradeResponse]):
        pass

    @abstractmethod
    def grade(
        self,
        qr: QuestionReport,
        q: QuestionAttempt,
        comment: str,
        mark: float,
        max_mark: float = None,
        comment_format: CommentFormat = None,
        **kwargs,
    ):
        pass


class BatchGrader:
    def __init__(
        self, moodle: Moodle, submit_after_num: int, submit_after_seconds: float
    ) -> None:
        self.moodle = moodle
        self.last_submit = time.time()
        self.submit_after_num = submit_after_num
        self.submit_after_seconds = submit_after_seconds
        self.current_batch = []

    def submit(self, gr: GradeResponse):
        self.current_batch.append(gr)
        if (
            self.submit_after_num > 0
            and len(self.current_batch) >= self.submit_after_num
            or self.submit_after_seconds > 0
            and (time.time() - self.last_submit) > self.submit_after_seconds
        ):
            self.submit_to_moodle()

    def submit_to_moodle(self):
        batch, self.current_batch = self.current_batch, []
        self.last_submit = time.time()
        self.moodle.post_grades(batch)

    def __enter__(self):
        assert self.moodle.batch_grader is None
        self.moodle.batch_grader = self
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self.current_batch:
            self.submit_to_moodle()
        self.moodle.batch_grader = None
