# Tested with moodle 3.9.6 as deployed by the Paderborn University
import logging
import re
from typing import Any, Dict, Iterable, Union

from bs4 import BeautifulSoup

from ...attempt import CommentFormat, QuestionAttempt, QuestionReport
from ..base_ import GradeMode, GradeResponse, Moodle
from .parser.question_report import QuestionReportParser

_LOGGER = logging.getLogger(__name__)
_SOUP_PARSER = "html.parser"


_RE_ATTEMPT_COUNT = re.compile(r"Grading attempts \d+ to \d+ of (?P<attempt_count>\d+)")

_MOD_QUIZ_REPORT_URL = "/mod/quiz/report.php"


class GradeResponse_3_9(GradeResponse):
    # meta; should be same for all requests
    quiz_id: int
    slot: int
    question_id: int
    # meta; required for serializing
    quba_id: int

    # meta; required but hidden from user
    sequencecheck: int
    comment_item_id: int
    min_fraction: float
    max_fraction: float

    comment: str
    comment_format: CommentFormat
    mark: float
    max_mark: float

    def __init__(self, *value_holders: Union[Any, Dict[str, Any]]) -> None:
        for member_name, clazz in self.__class__.__annotations__.items():
            for x in value_holders:
                if isinstance(x, dict) and member_name in x:
                    v = x[member_name]
                    break
                elif hasattr(x, member_name):
                    v = getattr(x, member_name)
                    break
            if not isinstance(v, clazz):
                v = clazz(v)
            setattr(self, member_name, v)
            del v


class Moodle_3_9(Moodle):
    def __init__(
        self,
        base_url: str,
        cookie: str,
        cookie_name: str,
        qreport_parser: QuestionReportParser = None,
    ) -> None:
        super().__init__(base_url=base_url, cookie=cookie, cookie_name=cookie_name)
        if qreport_parser is None:
            qreport_parser = QuestionReportParser(self)
        self.qreport_parser = qreport_parser

    def get_attempt_count(
        self,
        quiz_id: int,
        slot: int,
        question_id: int,
        grade_mode: GradeMode = GradeMode.NEED_GRADING,
    ) -> int:
        if not isinstance(grade_mode, GradeMode):
            grade_mode = GradeMode(grade_mode)
        _LOGGER.silly("Getting attempt count")
        res = self.get(
            _MOD_QUIZ_REPORT_URL,
            {
                "id": quiz_id,
                "mode": "grading",
                "slot": slot,
                "qid": question_id,
                "grade": grade_mode.value,
                "pagesize": "1",
            },
        )
        soup = BeautifulSoup(res.text, _SOUP_PARSER)
        count_tag = soup.find("h3", text=_RE_ATTEMPT_COUNT)
        m = _RE_ATTEMPT_COUNT.match(count_tag.text)
        c = m.group("attempt_count")
        _LOGGER.silly("Found attempt count %s", c)
        return int(c)

    def get_attempts(
        self,
        quiz_id: int,
        slot: int,
        question_id: int,
        grade_mode: GradeMode = GradeMode.NEED_GRADING,
    ) -> QuestionReport:
        if not isinstance(grade_mode, GradeMode):
            grade_mode = GradeMode(grade_mode)
        _LOGGER.silly(
            "Getting attempts for quiz %d, slot %d, question %d (%s)",
            quiz_id,
            slot,
            question_id,
            grade_mode.value,
        )
        res = self.get(
            _MOD_QUIZ_REPORT_URL,
            {
                "id": quiz_id,
                "mode": "grading",
                "slot": slot,
                "qid": question_id,
                "grade": grade_mode.value,
                "pagesize": self.get_attempt_count(
                    quiz_id, slot, question_id, grade_mode
                ),
            },
        )
        _LOGGER.silly("Got attempt")
        soup = BeautifulSoup(res.text, _SOUP_PARSER)
        res = self.qreport_parser.parse(soup)
        _LOGGER.silly("Parsed attempt")
        return res

    def _get_sesskey(self):
        _LOGGER.silly("Getting sesskey")
        res = self.get(
            "/",
        )
        soup = BeautifulSoup(res.text, _SOUP_PARSER)
        k = soup.find("input", {"type": "hidden", "name": "sesskey"})
        return k["value"]

    def post_grades(self, grades: Iterable[GradeResponse_3_9]):
        quiz_ids = set(map(lambda g: str(g.quiz_id), grades))
        slots = set(map(lambda g: str(g.slot), grades))
        question_ids = set(map(lambda g: str(g.question_id), grades))
        quba_ids = set(map(lambda g: str(g.quba_id), grades))

        # not sure if moodle handles multiple of these ids properly...
        assert (
            len(quiz_ids) == 1
        )  # only have one way to define quiz_id, probably shouldn't try multiple at once
        assert len(slots) == 1
        assert len(question_ids) == 1

        params = {
            "id": list(quiz_ids)[0],
            "mode": "grading",
            "grade": "all",
            "slot": list(slots)[0],
            "qid": list(question_ids)[0],
        }
        data = {
            "qubaids": ",".join(quba_ids),
            "slots": ",".join(slots),
            "sesskey": self._get_sesskey(),
        }
        _LOGGER.silly("Preparing to post %d grades", len(grades))
        for g in grades:
            prefix = f"q{g.quba_id}:{g.slot}_"
            prefixC = prefix + ":"  # control
            prefixB = prefix + "-"  # behavior
            data[prefixC + "sequencecheck"] = g.sequencecheck
            data[prefixB + "comment"] = g.comment
            data[prefixB + "comment:itemid"] = g.comment_item_id
            data[prefixB + "commentformat"] = g.comment_format.value
            data[prefixB + "mark"] = g.mark
            data[prefixB + "maxmark"] = g.max_mark
            data[prefixC + "minfraction"] = g.min_fraction
            data[prefixC + "maxfraction"] = g.max_fraction
        res = self.post(_MOD_QUIZ_REPORT_URL, data=data, params=params)
        _LOGGER.info("Posted %d grades", len(grades))

    def grade(
        self,
        qr: QuestionReport,
        q: QuestionAttempt,
        comment: str,
        mark: float,
        max_mark: float = None,
        comment_format: CommentFormat = None,
        **kwargs,
    ):
        kwargs["comment"] = comment
        kwargs["mark"] = mark
        if max_mark is not None:
            kwargs["max_mark"] = max_mark
        if comment_format is not None:
            kwargs["comment_format"] = comment_format

        assert 0 <= mark <= max_mark
        gr = GradeResponse_3_9(kwargs, q, qr)
        if self.batch_grader:
            self.batch_grader.submit(gr)
        else:
            self.post_grades([gr])
