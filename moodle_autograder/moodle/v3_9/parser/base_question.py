import re
from abc import ABC, abstractmethod

from bs4.element import Tag
from moodle_autograder.attempt import CommentFormat, QuestionAttempt, QuestionState
from moodle_autograder.moodle.base_ import Moodle


class QuestionParser(ABC):
    def __init__(self, moodle: Moodle) -> None:
        super().__init__()
        self.moodle = moodle

    def _parse_common_quba_and_slot(self, qdiv: Tag, ret: dict):
        assert "quba_id" not in ret
        assert "slot" not in ret
        assert qdiv["id"].startswith("question-")
        _q, quba_id, slot = qdiv["id"].split("-")
        ret["quba_id"] = int(quba_id)
        ret["slot"] = int(slot)

    def _parse_common_state(self, qdiv: Tag, ret: dict):
        assert "state" not in ret
        _que, _qtype, _qbehaviour, qstate = qdiv["class"]
        ret["state"] = QuestionState(qstate.lower())

    def _parse_common_qtext(self, qdiv: Tag, ret: dict):
        assert "qtext" not in ret
        qtext = qdiv.find_all("div", {"class": "qtext"})
        assert len(qtext) == 1
        ret["qtext"] = qtext[0]

    def _parse_common_input_values(self, qdiv: Tag, ret: dict):
        qubaid = ret["quba_id"]
        slot = ret["slot"]
        _quba_slot_prefix = f"q{qubaid}:{slot}_"
        for html_name, object_key, mapfunc in [
            (f"{_quba_slot_prefix}:sequencecheck", "sequencecheck", int),
            (f"{_quba_slot_prefix}-comment:itemid", "comment_item_id", int),
            (
                f"{_quba_slot_prefix}-mark",
                "current_mark",
                lambda x: float(x.replace(",", ".")) if x else None,
            ),
            (f"{_quba_slot_prefix}-maxmark", "max_mark", float),
            (f"{_quba_slot_prefix}:minfraction", "min_fraction", float),
            (f"{_quba_slot_prefix}:maxfraction", "max_fraction", float),
        ]:
            ret[object_key] = mapfunc(qdiv.find("input", {"name": html_name})["value"])

    def _parse_common_comment(self, qdiv: Tag, ret: dict):
        qubaid = ret["quba_id"]
        slot = ret["slot"]
        _quba_slot_prefix = f"q{qubaid}:{slot}_"
        for html_name, object_key in [
            (f"{_quba_slot_prefix}-comment", "current_comment"),
        ]:
            textarea = qdiv.find("textarea", {"name": html_name})
            ret[object_key] = "".join(map(str, textarea.contents))

    def _parse_common_commentformat(self, qdiv: Tag, ret: dict):
        qubaid = ret["quba_id"]
        slot = ret["slot"]
        _quba_slot_prefix = f"q{qubaid}:{slot}_"
        html_name, object_key, mapfunc = (
            f"{_quba_slot_prefix}-commentformat",
            "comment_format",
            lambda x: CommentFormat(int(x)),
        )
        type_input = qdiv.find("input", {"name": html_name})
        if type_input is None:
            type_input: Tag = qdiv.find("select", {"name": html_name})
            type_input = type_input.find("option", selected="selected")
        ret[object_key] = mapfunc(type_input["value"])

    def parse_common(self, qdiv: Tag, ret: dict):
        assert "attempt_number" in ret
        assert "student_name" in ret
        for parser in [
            self._parse_common_quba_and_slot,
            self._parse_common_input_values,
            self._parse_common_comment,
            self._parse_common_commentformat,
            self._parse_common_state,
            self._parse_common_qtext,
        ]:
            parser(qdiv, ret)

    @abstractmethod
    def parse(self, qdiv: Tag, ret: dict) -> QuestionAttempt:
        pass
