from bs4.element import Tag
from moodle_autograder.attempt import EssayAttempt

from .base_question import QuestionParser


class EssayParser(QuestionParser):
    def parse(self, qdiv: Tag, ret: dict) -> EssayAttempt:
        _que, _qtype, _qbehaviour, _qstate = qdiv["class"]
        assert _que == "que"
        assert _qtype == "essay"
        assert _qbehaviour == "manualgraded"
        assert _qstate == "complete"

        self.parse_common(qdiv, ret)

        assert len(qdiv.find_all("div", {"class": "answer"})) == 1
        assert len(qdiv.find_all("div", {"class": "attachments"})) == 1

        ret["answer"] = qdiv.find("div", {"class": "answer"})

        _attachments: Tag = qdiv.find("div", {"class": "attachments"})
        attachments = {}
        for el in _attachments.find_all("a"):
            content_req = self.moodle.get(el["href"])
            data = content_req.content
            _name = el.text.strip()
            i = 0
            name = _name
            while name in attachments:
                i += 1
                name = _name + "__" + str(i)
            attachments[name] = data
        ret["attachments"] = attachments

        return EssayAttempt(**ret)
