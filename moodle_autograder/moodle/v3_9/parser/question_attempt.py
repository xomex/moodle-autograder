import logging
import re
from typing import Dict

from bs4.element import Tag
from moodle_autograder.attempt import QuestionAttempt
from moodle_autograder.moodle.base_ import Moodle

from .base_question import QuestionParser
from .essay import EssayParser
from .shortanswer import ShortanswerParser

_LOGGER = logging.getLogger(__name__)

_DEFAULT_PARSER_CLASSES = {
    "essay": EssayParser,
    "shortanswer": ShortanswerParser,
}


class QuestionAttemptParser:
    def __init__(
        self, moodle: Moodle, parsers: Dict[str, QuestionParser] = None
    ) -> None:
        if parsers is None:
            parsers = {}
        else:
            parsers = dict(parsers)
        for k, cls in _DEFAULT_PARSER_CLASSES.items():
            if k not in parsers:
                parsers[k] = cls(moodle)
        self.PARSERS = parsers
        self.moodle = moodle

    def parse(self, qdiv: Tag, ret: dict) -> QuestionAttempt:
        _que, qtype, _qbehaviour, _qstate = qdiv["class"]
        assert _que == "que"
        parser = self.PARSERS.get(qtype)
        if parser is None:
            _LOGGER.warning(
                "Skipping question %s because we have no parser for %s",
                qdiv["id"],
                qtype,
            )
            return None
        return parser.parse(qdiv, ret)
