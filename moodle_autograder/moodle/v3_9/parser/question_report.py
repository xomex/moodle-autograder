import re
from typing import TYPE_CHECKING

from bs4 import BeautifulSoup
from bs4.element import Tag
from moodle_autograder.attempt import QuestionReport
from moodle_autograder.moodle.base_ import Moodle

from .question_attempt import QuestionAttemptParser

_RE_ATTEMPT_HEADER = re.compile(
    r"Attempt number (?P<attempt_number>\d+) for (?P<student_name>.+)"
)


class QuestionReportParser:
    def __init__(self, moodle: Moodle, qa_parser: QuestionAttemptParser = None) -> None:
        if qa_parser is None:
            qa_parser = QuestionAttemptParser(moodle)
        self.qa_parser = qa_parser
        self.moodle = moodle

    def _parse_options_form(self, soup: BeautifulSoup, ret: dict):
        form: Tag = soup.find("fieldset", id="id_options").parent
        assert form.name == "form"
        for html_name, object_name, mapfunc in [
            ("id", "quiz_id", int),
            ("slot", "slot", int),
            ("qid", "question_id", int),
        ]:
            assert object_name not in ret
            ret[object_name] = mapfunc(form.find("input", {"name": html_name})["value"])

    def parse(self, soup: BeautifulSoup) -> QuestionReport:
        ret = {}
        self._parse_options_form(soup, ret)

        attempts = []
        question_form: Tag = soup.find("form", id="manualgradingform")
        question_form_children = list(question_form.children)
        assert len(question_form_children) == 1
        question_div: Tag = question_form_children[0]
        for qheader_el in soup.find_all("h4", text=_RE_ATTEMPT_HEADER):
            if TYPE_CHECKING:
                qheader_el = Tag()
            base_info = _RE_ATTEMPT_HEADER.match(qheader_el.text).groupdict()
            base_info["attempt_number"] = int(base_info["attempt_number"])
            qbody_div = qheader_el.nextSibling
            assert qbody_div.name == "div" and qbody_div["id"].startswith("question-")
            attempts.append(self.qa_parser.parse(qbody_div, base_info))
        ret["attempts"] = attempts
        return QuestionReport(**ret)
