from bs4.element import Tag
from moodle_autograder.attempt import ShortanswerAttempt

from .base_question import QuestionParser


class ShortanswerParser(QuestionParser):
    def parse(self, qdiv: Tag, ret: dict) -> ShortanswerAttempt:
        _que, _qtype, _qbehaviour, _qstate = qdiv["class"]
        assert _que == "que"
        assert _qtype == "shortanswer"
        assert _qbehaviour == "deferredfeedback"
        assert _qstate in ["complete", "notanswered"]

        self.parse_common(qdiv, ret)

        assert len(qdiv.find_all("span", {"class": "answer"})) == 1

        answer_span: Tag = qdiv.find("span", {"class": "answer"})
        answer_input = answer_span.find("input")
        ret["answer"] = answer_input.get("value")

        return ShortanswerAttempt(**ret)
