import asyncio
import inspect

__all__ = ["_call_possibly_async"]


def _call_possibly_async(f, *args, **kwargs):
    """Helper function to call sync or async functions."""
    if inspect.iscoroutinefunction(f):
        return asyncio.get_event_loop().run_until_complete(f(*args, **kwargs))
    else:
        return f(*args, **kwargs)
