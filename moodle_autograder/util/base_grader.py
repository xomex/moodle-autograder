import logging
from abc import ABC, abstractmethod
from dataclasses import dataclass
from io import StringIO

from moodle_autograder.util.test_framework.test import TestParametersContextFrame, TestResults

from ..attempt import CommentFormat, QuestionAttempt, QuestionReport
from ..moodle.base_ import GradeMode, Moodle
from ._async import _call_possibly_async
from .test_framework import TestBase

_LOGGER = logging.getLogger(__name__)


@dataclass
class Grade:
    """A grade as specified by moodle. max_grade does not have to match the one set in moodle - it will be scaled accordingly."""

    grade: float
    max_grade: float


class BaseGrader(ABC):
    """Grader for a single question in moddle."""

    def __init__(self, moodle: Moodle, quiz_id: int, slot: int, question_id: int) -> None:
        self.moodle = moodle
        self.quiz_id = quiz_id
        self.slot = slot
        self.question_id = question_id
        self._enter_stack = 0
        self.cached_qr = None

    def get_attempts(self, grade_mode: GradeMode = GradeMode.NEED_GRADING) -> QuestionReport:
        """Get the attempts for the question. The GradeMode is passed to moodle."""
        if self.cached_qr is None:
            self.cached_qr = self.moodle.get_attempts(self.quiz_id, self.slot, self.question_id, grade_mode)
        return self.cached_qr

    def gradeAll(self, grade_mode: GradeMode = GradeMode.NEED_GRADING):
        """Grade all attempts for the question. Combines get_attempts and grade."""
        if self.moodle.batch_grader is None:
            _LOGGER.warning("Using gradeAll without a batch_grade registered in moodle; performance might be bad")
        qr = self.get_attempts(grade_mode)
        with self as _:
            for qa in qr.attempts:
                self.grade(qr, qa)

    def grade(self, qr: QuestionReport, qa: QuestionAttempt):
        """Grade a single attempt using grade_internal. Captures logging output as feedback."""
        _logger = logging.root
        with StringIO() as logs_buffer:
            stream = logging.StreamHandler(logs_buffer)
            stream.setLevel("SILLY")
            formatter = logging.Formatter("%(asctime)s %(levelname)8s | %(name)s: %(message)s")
            stream.setFormatter(formatter)
            _logger.addHandler(stream)
            _LOGGER.info("Start grading Attempt %d of %s", qa.attempt_number, qa.student_name)
            try:
                grade = _call_possibly_async(self.grade_internal, qr, qa)
            except Exception:
                _LOGGER.exception("Exception while grading, treating as 0")
                grade = Grade(0, qa.max_mark)
            _LOGGER.info("Done grading Attempt %d of %s", qa.attempt_number, qa.student_name)
            if qa.current_mark is not None and grade is not None:
                change = (grade.grade / grade.max_grade) - (qa.current_mark / qa.max_mark)
                _LOGGER.info(
                    "Old mark: %.2f/%.2f, New mark %.2f/%.2f (absolute change: %.2f%%)",
                    qa.current_mark,
                    qa.max_mark,
                    grade.grade,
                    grade.max_grade,
                    100 * change,
                )
            _logger.removeHandler(stream)
            logs = logs_buffer.getvalue()
            logs = f"```log\n{logs}\n```"
        if grade is not None:
            self.moodle.grade(
                qr,
                qa,
                logs,
                grade.grade,
                grade.max_grade,
                CommentFormat.FORMAT_MARKDOWN,
            )
        else:
            _LOGGER.warning("Got None grade - ignoring")

    def __enter__(self):
        if self._enter_stack == 0:
            _call_possibly_async(self.initialize)
        self._enter_stack += 1

    def __exit__(self, exc_type, exc_val, exc_tb):
        self._enter_stack -= 1
        if self._enter_stack == 0:
            _call_possibly_async(self.deinitialize)

    @abstractmethod
    def initialize(self):
        pass

    @abstractmethod
    def deinitialize(self):
        pass

    @abstractmethod
    def grade_internal(self, qr: QuestionReport, qa: QuestionAttempt) -> Grade:
        pass


class TestBasedGrader(BaseGrader, TestBase):
    def __init__(
        self,
        moodle: Moodle,
        quiz_id: int,
        slot: int,
        question_id: int,
        remember_results: bool = True,
    ) -> None:
        BaseGrader.__init__(self, moodle, quiz_id, slot, question_id)
        TestBase.__init__(self, remember_results)

    def grade_internal(self, qr: QuestionReport, qa: QuestionAttempt) -> Grade:
        additional_fixtures = {
            "question_report": qr,
            "question_attempt": qa,
        }
        return self.run_all_tests(additional_fixtures)

    @abstractmethod
    def evaluate(self, results: TestResults, context: TestParametersContextFrame) -> Grade:
        pass

    def deinitialize(self):
        # Not needed - use fixtures
        pass

    def initialize(self):
        # Not needed - use fixtures
        pass
