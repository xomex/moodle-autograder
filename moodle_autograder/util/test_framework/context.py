"""Define a context frame.

The context contains all fixtures that are available to a test.
Contexts are hierarchical. I.e. if a fixture is not found in the current context, it is searched in the parent context.
"""
import logging
from enum import Enum
from typing import Any, Dict, Optional

from moodle_autograder.util.test_framework.resource_holders import ResourceHolder

_LOGGER = logging.getLogger(__name__)


class ContextType(Enum):
    RUNTIME = 0
    TEST_OBJECT = 1
    PARAMETER = 2  # one call to grade_all
    STAGE = 3
    FUNCTION = 4  # a single function


class _SpecialResourceValues(Enum):
    NOT_FOUND_CHECK_PARENT = "NOT_FOUND_CHECK_PARENT"
    NOT_FOUND_ANYWHERE = "NOT_FOUND_ANYWHERE"


class ContextFrame:
    def __init__(self, parent: "ContextFrame") -> None:
        if parent is None:
            self.ctype = ContextType.RUNTIME
        else:
            self.ctype = ContextType(parent.ctype.value + 1)
        self.parent = parent
        self._resources: Optional[Dict[str, ResourceHolder]] = None
        assert (self.parent is None) == (self.ctype == ContextType.RUNTIME)

    def _on_child_enter(self, child, *intermediate_children):
        # event subclasses can hook into
        if self.parent:
            self.parent._on_child_enter(child, *intermediate_children, self)

    def _on_child_exit(self, child, *intermediate_children):
        # events subclasses can hook into
        if self.parent:
            self.parent._on_child_exit(child, *intermediate_children, self)

    def __enter__(self):
        assert self._resources is None
        self._resources = {}
        if self.parent:
            self.parent._on_child_enter(self)
        return self

    def get(self, key: str, default: Any = ...) -> ResourceHolder:
        assert isinstance(key, str)
        if key in self._resources:
            return self._resources[key]
        if self.parent:
            return self.parent.get(key, default)
        if default != ...:
            return default
        raise IndexError("Invalid index", key)

    def __getitem__(self, key: str):
        return self.get(key)

    def register_resource(self, key: str, value: ResourceHolder):
        assert isinstance(key, str)
        assert isinstance(value, ResourceHolder)
        if key in self._resources:
            raise IndexError("Key already present")
        self._resources[key] = value

    def __setitem__(self, key: str, value: ResourceHolder):
        return self.register_resource(key, value)

    def get_frame(self, typ: ContextType) -> "ContextFrame":
        if self.ctype == typ:
            return self
        if self.parent:
            return self.parent.get_frame(typ)

    def __getattr__(self, name: str) -> Any:
        if self.parent:
            return getattr(self.parent, name)
        raise AttributeError(f"ContextFrame could not find {name!r} attribute in itself or its parents")

    def __exit__(self, exc_type, exc_value, exc_traceback):
        assert isinstance(self._resources, Dict)
        resources, self._resources = self._resources, None
        for r in resources.values():
            r.free_value()
        if self.parent:
            self.parent._on_child_exit(self)

    def __repr__(self) -> str:
        return f"<ContextFrame {self.ctype!s}>"
