import asyncio
import inspect
import logging
from abc import ABC, abstractmethod
from dataclasses import dataclass
from typing import Any, AsyncGenerator, Generator, List, Union

from moodle_autograder.util.test_framework.context import ContextFrame
from moodle_autograder.util.test_framework.resource_holders import ResourceHolder

from .._async import _call_possibly_async

_LOGGER = logging.getLogger(__name__)


class _NotFound:
    pass


class RequirementsCannotBeMetException(Exception):
    pass


class RequirementsNotYetMetException(Exception):
    pass


@dataclass
class RequirementFunctionResourceHolder(ResourceHolder):
    requirement_function: "RequirementsDecorator"
    value: Any = None
    fetched_value: bool = False

    def get_value(self, context) -> Any:
        if not self.fetched_value:
            self.value = self.requirement_function(context)
            self.fetched_value = True
        return self.value

    def free_value(self):
        # don't have to free constant
        pass

    def can_still_resolve(self) -> bool:
        return self.requirement_function.can_still_resolve(self.context)

    def can_now_resolve(self) -> bool:
        return self.requirement_function.can_now_resolve(self.context)

    def is_resolved(self) -> bool:
        return self.fetched_value


@dataclass
class _BaseRequirementGeneratorFunctionHolder(RequirementFunctionResourceHolder):
    generator: Union[Generator, AsyncGenerator] = None
    fetched_generator: bool = False
    freed_value: bool = False

    @abstractmethod
    def _next(self) -> Any:
        pass

    def get_value(self, context) -> Any:
        if not self.fetched_value:
            assert not self.fetched_generator
            self.generator = super().get_value(context)
            self.value = self._next()
            self.fetched_generator = True
        return self.value

    def free_value(self):
        if self.fetched_value and not self.freed_value:
            assert self.fetched_generator
            self.free_value = True
            try:
                self._next()
            except (StopIteration, StopAsyncIteration):
                pass
            else:
                _LOGGER.error(
                    "Could not free value - generator returned more than one item"
                )


@dataclass
class SyncRequirementGeneratorFunctionHolder(_BaseRequirementGeneratorFunctionHolder):
    def _next(self) -> Any:
        return next(self.generator)


@dataclass
class AsyncRequirementGeneratorFunctionHolder(_BaseRequirementGeneratorFunctionHolder):
    @staticmethod
    async def _anext(gen):
        return await gen.__anext__()

    def _next(self) -> Any:
        return asyncio.get_event_loop().run_until_complete(
            AsyncRequirementGeneratorFunctionHolder._anext(self.generator)
        )


class RequirementsDecorator(ABC):
    def __init__(self, requirements: List, f) -> None:
        self.f = f
        self.sig = inspect.signature(f)
        self.requirements = requirements
        self.__name__ = self.f.__name__
        self.__doc__ = self.f.__doc__

    def _get_requirement_from_context(
        self, req, context: ContextFrame
    ) -> ResourceHolder:
        if isinstance(req, str):
            ret = context.get(req, default=_NotFound)
            if ret is not _NotFound:
                return ret
        if hasattr(req, "__name__"):
            ret = context.get(req.__name__, default=_NotFound)
            if ret is not _NotFound:
                return ret
        return _NotFound

    def _get_first_req_cannot_be_met(self, context: ContextFrame, requirements):
        for r in requirements:
            rh = self._get_requirement_from_context(r, context)
            if rh == _NotFound:
                _LOGGER.warning("Could not find requirement %r", r)
                return r
            if not rh.can_still_resolve():
                return r

    def _get_first_req_not_yet_met(self, context: ContextFrame, requirements):
        for r in requirements:
            rh = self._get_requirement_from_context(r, context)
            if rh == _NotFound:
                _LOGGER.warning("Could not find requirement %r", r)
                return r
            if not rh.can_now_resolve():
                return r

    def can_still_resolve(self, context: ContextFrame):
        r = self._get_first_req_cannot_be_met(context, self.requirements)
        if r is not None:
            return False
        r = self._get_first_req_cannot_be_met(context, self.sig.parameters)
        if r is not None:
            return False
        return True

    def can_now_resolve(self, context: ContextFrame):
        r = self._get_first_req_not_yet_met(context, self.requirements)
        if r is not None:
            return False
        r = self._get_first_req_not_yet_met(context, self.sig.parameters)
        if r is not None:
            return False
        return True

    def _assert_requirements_met_internal(
        self, context: ContextFrame, requirements, exc_prefix
    ):
        r = self._get_first_req_cannot_be_met(context, requirements)
        if r is not None:
            raise RequirementsCannotBeMetException(
                f"{exc_prefix} requirement cannot be met", r
            )
        r = self._get_first_req_not_yet_met(context, requirements)
        if r is not None:
            raise RequirementsNotYetMetException(
                f"{exc_prefix} requirement are not yet met", r
            )

    def assert_requirements_met(self, context: ContextFrame):
        self._assert_requirements_met_internal(context, self.requirements, "Explicit")
        self._assert_requirements_met_internal(context, self.sig.parameters, "Implicit")

    def _call_internal(self, kwargs):
        return _call_possibly_async(self.f, **kwargs)

    def __call__(self, context: ContextFrame) -> Any:
        self.assert_requirements_met(context)
        # get parameter values
        kwargs = {}
        for r, desc in self.sig.parameters.items():
            rh = self._get_requirement_from_context(r, context)
            v = rh.get_value(context)
            if desc.annotation != inspect.Parameter.empty and not isinstance(
                v, desc.annotation
            ):
                _LOGGER.warning(
                    "Could not get correct type for requirement %r: Got %r, Expected %r",
                    r,
                    type(v),
                    desc.annotation,
                )
            kwargs[r] = v
        return self._call_internal(kwargs)
