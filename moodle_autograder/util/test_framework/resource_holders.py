import logging
from abc import abstractmethod
from dataclasses import dataclass
from typing import Any

from moodle_autograder.util.test_framework import context

_LOGGER = logging.getLogger(__name__)


@dataclass
class ResourceHolder:
    context: "context.ContextFrame"

    @abstractmethod
    def can_still_resolve(self) -> bool:
        pass

    @abstractmethod
    def can_now_resolve(self) -> bool:
        pass

    @abstractmethod
    def is_resolved(self) -> bool:
        pass

    @abstractmethod
    def get_value(self, context) -> Any:
        pass

    @abstractmethod
    def free_value(self) -> None:
        pass


@dataclass
class LiteralResourceHolder(ResourceHolder):
    value: Any

    def get_value(self, context) -> Any:
        return self.value

    def free_value(self):
        # don't have to free literals
        pass

    def can_still_resolve(self) -> bool:
        return True

    def can_now_resolve(self) -> bool:
        return True

    def is_resolved(self) -> bool:
        return True
