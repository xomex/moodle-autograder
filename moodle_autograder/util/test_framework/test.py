import atexit
import inspect
import logging
from abc import abstractmethod
from dataclasses import dataclass
from functools import partial
from typing import Any, Dict, Iterable, List, Optional, Tuple

from moodle_autograder.util.test_framework.requirements import (
    AsyncRequirementGeneratorFunctionHolder,
    RequirementFunctionResourceHolder,
    RequirementsCannotBeMetException,
    RequirementsDecorator,
    RequirementsNotYetMetException,
    SyncRequirementGeneratorFunctionHolder,
)

from .context import ContextFrame, ContextType
from .resource_holders import LiteralResourceHolder, ResourceHolder

_LOGGER = logging.getLogger(__name__)

RUNTIME_CONTEXT = ContextFrame(None)
RUNTIME_CONTEXT.__enter__()
atexit.register(RUNTIME_CONTEXT.__exit__, None, None, None)


def _name(x):
    return x.__name__


@dataclass
class TestResult:
    got_exception: bool
    return_value: Optional[Any]
    exception: Optional[Exception]

    @staticmethod
    def from_retval(rv) -> "TestResult":
        return TestResult(False, rv, None)

    @staticmethod
    def from_exc(exc) -> "TestResult":
        return TestResult(True, None, exc)


class FixtureDecorator(RequirementsDecorator):
    def __init__(self, lifetime: ContextType, requirements: List, f) -> None:
        super().__init__(requirements, f)
        self.lifetime = lifetime
        if self.lifetime == ContextType.RUNTIME:
            self.register(RUNTIME_CONTEXT)

    def register(self, context: ContextFrame):
        cls = None
        if inspect.isgeneratorfunction(self.f):
            cls = SyncRequirementGeneratorFunctionHolder
        elif inspect.isasyncgenfunction(self.f):
            cls = AsyncRequirementGeneratorFunctionHolder
        elif inspect.isfunction(self.f) or inspect.ismethod(self.f):
            cls = RequirementFunctionResourceHolder
        else:
            raise Exception("Cannot handle annotated function", self.f)
        rh = cls(context, self)
        context.register_resource(self.f.__name__, rh)

    def __repr__(self) -> str:
        return f"<Fixture {self.lifetime!s} {self.f!r}>"


@dataclass
class TestResourceHolder(ResourceHolder):
    test_function: "TestDecorator"

    @property
    def value(self) -> Optional[TestResult]:
        name = self.test_function.__name__
        return self.context.test_results.get(name, None)

    @property
    def was_run(self):
        return self.value is not None

    @abstractmethod
    def can_still_resolve(self) -> bool:
        if self.was_run:
            return not self.value.got_exception
        else:
            return self.test_function.can_still_resolve(self.context)

    @abstractmethod
    def can_now_resolve(self) -> bool:
        return self.was_run

    @abstractmethod
    def is_resolved(self) -> bool:
        return self.was_run

    @abstractmethod
    def get_value(self, context) -> Any:
        return self.value

    def free_value(self) -> None:
        # nothing to do
        pass


class TestDecorator(FixtureDecorator):
    def __init__(self, requirements: List, f, stage: int) -> None:
        super().__init__(ContextType.PARAMETER, requirements, f)
        self.stage = stage

    def __repr__(self) -> str:
        return f"<Test {self.f!r}>"

    def __call__(self, context: ContextFrame) -> TestResult:
        try:
            return super().__call__(context)
        except (RequirementsCannotBeMetException, RequirementsNotYetMetException) as e:
            _LOGGER.error("Requirements exception while running '%s': %r", self.__name__, e)
            return TestResult.from_exc(e)

    def _call_internal(self, kwargs) -> TestResult:
        try:
            _LOGGER.silly("Running '%s'", self.__name__)
            retcode = super()._call_internal(kwargs)
            res = TestResult.from_retval(retcode)
            _LOGGER.info("Ran '%s' (without exception)", self.__name__)
            return res
        except Exception as e:
            _LOGGER.error("Exception while running '%s': %r", self.__name__, e)
            return TestResult.from_exc(e)

    def register(self, context: ContextFrame):
        context.register_resource(self.f.__name__, TestResourceHolder(context, self))


def fixture(lifetime: ContextType = ContextType.FUNCTION, *requirements):
    if not isinstance(lifetime, ContextType):
        requirements = [lifetime, *requirements]
        lifetime = ContextType.FUNCTION
    return partial(FixtureDecorator, lifetime, requirements)


def test(*requirements, stage: int = 1):
    return partial(TestDecorator, requirements, stage=stage)


class TestResults:
    def __init__(self) -> None:
        self._dict = {}

    def _ensure_str(self, k) -> str:
        if isinstance(k, str):
            return k
        return k.__name__

    def __setitem__(self, key, value: TestResult) -> None:
        assert isinstance(value, TestResult)
        self._dict[self._ensure_str(key)] = value

    def __getitem__(self, key) -> TestResult:
        return self._dict[self._ensure_str(key)]

    def __contains__(self, key) -> bool:
        return self._ensure_str(key) in self._dict

    def __iter__(self):
        return self._dict.__iter__

    def items(self):
        return self._dict.items()

    def keys(self):
        return self._dict.keys()

    def values(self):
        return self._dict.values()

    def get(self, key, default: TestResult) -> TestResult:
        return self._dict.get(self._ensure_str(key))


class TestObjectContextFrame(ContextFrame):
    def __init__(self, parent: "ContextFrame", test_object: "TestBase") -> None:
        super().__init__(parent)
        self.test_object = test_object

    def _on_child_enter(self, child: ContextFrame, *intermediate_children):
        r = super()._on_child_enter(child, *intermediate_children)
        # register fixtures (N.B. Tests are also fixtures)
        fixtures: Iterable[FixtureDecorator] = list(filter(lambda x: x.lifetime == child.ctype, self.test_object.fixtures))
        for f in fixtures:
            f.register(child)
        return r

    def __enter__(self):
        r = super().__enter__()
        self._on_child_enter(self)
        return r


class TestParametersContextFrame(ContextFrame):
    def __init__(self, parent: "ContextFrame") -> None:
        super().__init__(parent)
        self.test_results = TestResults()


def _get_attributes_matching(predicate, object) -> Iterable[Any]:
    for k in dir(object):
        v = getattr(object, k)
        if predicate(v):
            yield v


class TestBase:
    def __init__(self, remember_results: bool = True) -> None:
        all_tests: List[TestDecorator] = list(_get_attributes_matching(lambda x: isinstance(x, TestDecorator), self))
        self.tests: Dict[int, List[TestDecorator]] = dict()
        for t in all_tests:
            assert isinstance(t.stage, int)
            if t.stage not in self.tests:
                self.tests[t.stage] = []
            self.tests[t.stage].append(t)

        self.fixtures: List[FixtureDecorator] = list(_get_attributes_matching(lambda x: isinstance(x, FixtureDecorator), self))
        self.context_frame = TestObjectContextFrame(RUNTIME_CONTEXT, self)
        self.context_frame.__enter__()
        self.context_frame.register_resource("self", LiteralResourceHolder(self.context_frame, self))
        self.results_history = None
        if remember_results:
            self.results_history: List[Tuple[Any, TestResults]] = []  # Lists additional_fixtures, results

    def __del__(self):
        self.context_frame.__exit__(None, None, None)

    def __run_test(
        self,
        t: TestDecorator,
        parent_context: TestParametersContextFrame,
        force: bool = False,
    ):
        with ContextFrame(parent_context) as context:
            context: TestParametersContextFrame
            if not force and not t.can_now_resolve(context):
                return False
            if force:
                _LOGGER.info("Forcing to run '%s'", t.__name__)
            context.test_results[t] = t(context)
            return True

    def __run_stage(self, stage: List[TestDecorator], parent_context: TestParametersContextFrame):
        with ContextFrame(parent_context) as context:
            context: TestParametersContextFrame
            tests_to_run = list(stage)
            while tests_to_run:
                skipped = []
                ran = []
                for t in tests_to_run:
                    consider_run = self.__run_test(t, context)
                    if consider_run:
                        ran.append(t)
                    else:
                        skipped.append(t)
                if not ran:
                    # all tests skipped - no more left to run
                    _LOGGER.warning("Some tests were skipped - forcing to run them to get reason")
                    for t in skipped:
                        _LOGGER.warning("Skipped: %r", _name(t))
                        self.__run_test(t, context, True)
                    break
                for t in ran:
                    tests_to_run.remove(t)

    def run_all_tests(self, additional_fixtures: Dict[str, Any]):
        try:
            assert False
        except AssertionError:
            pass
        else:
            raise Exception("Asserts are disabled")
        with TestParametersContextFrame(self.context_frame) as context:
            for k, v in additional_fixtures.items():
                context.register_resource(k, LiteralResourceHolder(context, v))

            for stage in sorted(self.tests):
                _LOGGER.silly("Running Test Stage %d", stage)
                tests = self.tests[stage]
                self.__run_stage(tests, context)
                _LOGGER.silly("Done with Test Stage %d", stage)

            if self.results_history is not None:
                self.results_history.append((additional_fixtures, context.test_results))
            return self.evaluate(context.test_results, context)

    def classify_results(
        self,
        initial_key_vector: Iterable[str] = None,
        results_history: List[Tuple[Any, TestResults]] = None,
    ):
        if results_history is None:
            results_history = self.results_history
        if initial_key_vector is None:
            initial_key_vector = []
        keys = set()
        for _af, test_results in results_history:
            for k in test_results.keys():
                keys.add(k)
        key_vector = list(initial_key_vector)
        key_vector.extend(keys - set(key_vector))
        keys = tuple(key_vector)
        classes = {None: keys}
        for _af, test_results in results_history:
            vector = []
            for k in keys:
                tr = test_results.get(k, None)
                if tr is None:
                    vector.append(None)
                else:
                    vector.append(not tr.got_exception)
            assert len(vector) == len(keys)
            vector = tuple(vector)
            if vector not in classes:
                classes[vector] = 0
            classes[vector] += 1
        return classes

    @abstractmethod
    def evaluate(self, results: TestResults, context: TestParametersContextFrame):
        pass
