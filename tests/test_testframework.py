from moodle_autograder.util.test_framework import ContextType
from moodle_autograder.util.test_framework import TestBase as _TestBase
from moodle_autograder.util.test_framework import TestResults as _TestResults
from moodle_autograder.util.test_framework import fixture, test


def test_dependencies():
    class DummyTestClass_Dependencies(_TestBase):
        def __init__(self) -> None:
            super().__init__()

        @test()
        def failing(self):
            assert False

        @test()
        def succeeding(self):
            assert True

        @test(failing)
        def notExecuted(self):
            assert True

        @test(notExecuted)
        def notExecuted2(self):
            assert True

        @test(succeeding)
        def executed(self):
            assert True

        @fixture(failing)
        def notExecutedFixture(self):
            return 3

        @test()
        def notExecutedDueFixture(self, notExecutedFixture):
            assert notExecutedFixture == 3

        @fixture(succeeding)
        def executedFixture(self):
            return 4

        @test()
        def isExecutedDueFixture(self, executedFixture):
            assert executedFixture == 4

        def evaluate(self, results, context):
            return results

    c = DummyTestClass_Dependencies()
    res: _TestResults = c.run_all_tests({"foo": "a"})
    assert res[c.succeeding].got_exception == False
    assert res[c.executed].got_exception == False
    assert res[c.failing].got_exception == True
    assert res[c.notExecuted].got_exception == True
    assert res[c.notExecuted2].got_exception == True
    assert res[c.notExecutedDueFixture].got_exception == True
    assert res[c.isExecutedDueFixture].got_exception == False


def test_adependencies():
    class DummyTestClass_ADependencies(_TestBase):
        def __init__(self) -> None:
            super().__init__()

        @test()
        async def failing(self):
            assert False

        @test()
        async def succeeding(self):
            assert True

        @test(failing)
        async def notExecuted(self):
            assert True

        @test(notExecuted)
        async def notExecuted2(self):
            assert True

        @test(succeeding)
        async def executed(self):
            assert True

        @fixture(failing)
        async def notExecutedFixture(self):
            return 3

        @test()
        async def notExecutedDueFixture(self, notExecutedFixture):
            assert notExecutedFixture == 3

        @fixture(succeeding)
        async def executedFixture(self):
            return 4

        @test()
        async def isExecutedDueFixture(self, executedFixture):
            assert executedFixture == 4

        def evaluate(self, results, context):
            return results

    c = DummyTestClass_ADependencies()
    res: _TestResults = c.run_all_tests({"foo": "a"})
    assert res[c.succeeding].got_exception == False
    assert res[c.executed].got_exception == False
    assert res[c.failing].got_exception == True
    assert res[c.notExecuted].got_exception == True
    assert res[c.notExecuted2].got_exception == True
    assert res[c.notExecutedDueFixture].got_exception == True
    assert res[c.isExecutedDueFixture].got_exception == False


def test_caching():
    class DummyTestClass_Caching(_TestBase):
        _counter_once = 0
        _counter_twice = 0
        _counter_thrice = 0
        _counter_anew = 0

        def __init__(self) -> None:
            super().__init__()

        @staticmethod
        @fixture(ContextType.RUNTIME)
        def fetched_once():
            DummyTestClass_Caching._counter_once += 1
            return DummyTestClass_Caching._counter_once

        @fixture(ContextType.TEST_OBJECT)
        def fetched_twice(self):
            DummyTestClass_Caching._counter_twice += 1
            return DummyTestClass_Caching._counter_twice

        @fixture(ContextType.PARAMETER)
        def fetched_thrice(self, foo):
            DummyTestClass_Caching._counter_thrice += 1
            return DummyTestClass_Caching._counter_thrice

        @fixture(ContextType.FUNCTION)
        def fetched_anew(self):
            DummyTestClass_Caching._counter_anew += 1
            return DummyTestClass_Caching._counter_anew

        @test()
        def fetcher(self, fetched_once, fetched_twice, fetched_thrice, fetched_anew):
            # nothing to do
            return fetched_once, fetched_twice, fetched_thrice, fetched_anew

        @test()
        def once_fetcher(self, fetched_once):
            assert fetched_once == 1

        @test()
        def anew_fetcher(self, fetched_anew):
            assert type(1) == type(fetched_anew)

        def evaluate(self, results, context):
            return results

    c1 = DummyTestClass_Caching()
    c1.run_all_tests({"foo": "a"})
    c1.run_all_tests({"foo": "a"})
    c2 = DummyTestClass_Caching()
    res: _TestResults = c2.run_all_tests({"foo": "c"})
    # check caching
    assert DummyTestClass_Caching._counter_once == 1
    assert DummyTestClass_Caching._counter_twice == 2
    assert DummyTestClass_Caching._counter_thrice == 3
    assert DummyTestClass_Caching._counter_anew == 6


def test_classification():
    class BitChecker(_TestBase):
        @test()
        def check_a_and_b_eq_b(self, a, b):
            assert (a & b) == b

        @test()
        def check_a_or_b_eq_b(self, a, b):
            assert (a | b) == b

        @test()
        def check_a_xor_b_eq_zero(self, a, b):
            assert (a ^ b) == 0

        def evaluate(self, results, context):
            return results

    c = BitChecker()
    c.run_all_tests({"a": 3, "b": 1})
    c.run_all_tests({"a": 3, "b": 3})
    c.run_all_tests({"a": 1, "b": 3})
    c.run_all_tests({"a": 5, "b": 1})
    c.run_all_tests({"a": 5, "b": 9})
    c.run_all_tests({"a": 2, "b": 1})
    classification = c.classify_results()
    assert set(classification[None]) == set(
        ["check_a_and_b_eq_b", "check_a_or_b_eq_b", "check_a_xor_b_eq_zero"]
    )

    kv = ("check_a_and_b_eq_b", "check_a_or_b_eq_b", "check_a_xor_b_eq_zero")
    classification = c.classify_results(kv)
    assert kv == ("check_a_and_b_eq_b", "check_a_or_b_eq_b", "check_a_xor_b_eq_zero")
    assert classification[None] == kv

    kv = ("check_a_xor_b_eq_zero", "check_a_and_b_eq_b", "check_a_or_b_eq_b")
    classification = c.classify_results(list(kv))
    assert classification[None] == kv

    kv = ("check_a_or_b_eq_b", "check_a_and_b_eq_b")
    classification = c.classify_results(kv)
    assert classification[None][:2] == kv


def test_generators():
    class Generators(_TestBase):
        def __init__(self) -> None:
            super().__init__()
            self.a_init = False
            self.a_deinit = False
            self.b_init = False
            self.b_deinit = False

        @fixture(ContextType.PARAMETER)
        def a(self):
            self.a_init = True
            yield 1
            self.a_deinit = True

        @fixture(ContextType.PARAMETER)
        async def b(self):
            self.b_init = True
            yield 1
            self.b_deinit = True

        @test()
        def consumer(self, a, b):
            assert True

        def evaluate(self, results, context):
            return results

    c = Generators()
    c.run_all_tests({})
    assert c.a_init
    assert c.a_deinit
    assert c.b_init
    assert c.b_deinit


def test_stages():
    class Generators(_TestBase):
        def __init__(self) -> None:
            super().__init__()
            self.last_run = False

        @test()
        def a(self):
            assert not self.last_run

        @test(a)
        def b(self):
            assert not self.last_run

        @test(b)
        def c(self):
            assert not self.last_run

        @test(stage=2)
        def last(self):
            self.last_run = True

        def evaluate(self, results, context):
            return results

    c = Generators()
    res: _TestResults = c.run_all_tests({})
    assert not res[c.a].got_exception
    assert not res[c.b].got_exception
    assert not res[c.c].got_exception
    assert not res[c.last].got_exception
